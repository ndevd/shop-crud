<?php

    include("conexion.php");
    $conex = conectar();
    $query = "SELECT * FROM producto";
    $res = mysqli_query($conex, $query);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mostrar artículos</title>
    <link rel="shortcut icon" type="image/x-icon" href="./img/bag.png">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <table border="2">
        <h1>Lista de artículos</h1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Rubro</th>
                <th>Descripción</th>
                <th>Precio</th>
                <th>Id Proveedor</th>
                <th colspan="2"></th>
            </tr>
        </thead>
        <tbody>
            <?php while($fila = mysqli_fetch_array($res)) :?>
            <tr>
                <th><?= $fila['id_articulo']?></th>
                <th><?= $fila['nombre']?></th>
                <th><?= $fila['rubro']?></th>
                <th><?= $fila['descripcion']?></th>
                <th><?= $fila['precio_unitario']?></th>
                <th><?= $fila['id_proveedor']?></th>
                <th><a href="editar.php?id=<?=$fila['id_articulo'] ?>"><button>Editar</button></a></th>
                <th><a><button onclick = "confirmar(<?=$fila['id_articulo'] ?>);">Eliminar</button></a></th>
            </tr>
            <?php endwhile;?>
        </tbody>
    </table>

    <script src="./main.js"></script>
</body>
</html>