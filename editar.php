<?php

include("conexion.php");
    $id = $_GET['id'];
    $conex = conectar();
    $query = "SELECT * FROM producto WHERE id_articulo = '$id'";
    $res = mysqli_query($conex, $query);
    $fila = mysqli_fetch_array($res);

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar artículo</title>
    <link rel="shortcut icon" type="image/x-icon" href="./img/bag.png">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <h1>TIENDA</h1>

    <div>
        <form action="actualizar.php" method="post">

            <h2>Editar producto</h2>

            <p>
                <input type="hidden" name="id" value="<?= $fila['id_articulo']?>">
            </p>
            <p>
                <input type="text" name="nombre" value="<?= $fila['nombre']?>" placeholder="Nombre..." 
                pattern="[A-Za-z]+"required>
            </p>
            <p>
                <input type="text" name="rubro" value="<?= $fila['rubro']?>" placeholder="Rubro..." 
                pattern="[A-Za-z]+"required>
            </p>
            <p>
                <textarea name="descripcion" placeholder="Descripción..."cols="30" rows="10" 
                    pattern="[A-Za-z]+" required><?= $fila['descripcion']?></textarea>
            </p>
            <p>
                <input type="number" name="precio-unitario" id="input-precio" placeholder="Precio..." min="0"
                    pattern="[0-9]+" title="Precio unitario" value="<?= $fila['precio_unitario']?>" required>
            </p>
            <p>
                <input type="number" name="id-proveedor" id="input-proveedor" placeholder="Nro id proveedor" min="0"
                    pattern="[0-9]+" title="Numero id de proveedor" value="<?= $fila['id_proveedor']?>" required>
            </p>
            <br>
            <p>
                <input type="submit" value="Actualizar">
            </p>
        </form>
    </div>
</body>

</html>