<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tienda</title>
    <link rel="shortcut icon" type="image/x-icon" href="./img/bag.png">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <h1>TIENDA</h1>

    <button id="boton-mostrar"><a href="mostrar.php" target="_blank">Mostrar todos los artículos</a></button>

    <div>
        <form action="registrar.php" method="post">
            <h2>Registrar producto</h2>

            <p>
                <input type="text" name="nombre" placeholder="Nombre..." pattern="[A-Za-z]+" autofocus required>
            </p>
            <p>
                <input type="text" name="rubro" placeholder="Rubro..." pattern="[A-Za-z]+" required>
            </p>
            <p>
                <textarea name="descripcion" placeholder="Descripción..." cols="30" rows="10" pattern="[A-Za-z]+"
                    required></textarea>
            </p>
            <p>
                <input type="number" name="precio-unitario" id="input-precio" placeholder="Precio..." min="0"
                    pattern="[0-9]+" title="Precio unitario" required>
            </p>
            <p>
                <input type="number" name="id-proveedor" id="input-proveedor" placeholder="Nro id proveedor" min="0"
                    pattern="[0-9]+" title="Numero id de proveedor" required>
            </p>
            <br>
            <p>
                <input type="submit" value="Guardar">
            </p>
        </form>
    </div>
</body>

</html>