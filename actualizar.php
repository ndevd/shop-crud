<?php

    include("conexion.php");
    $conex = conectar();

    if( !empty(trim($_POST['nombre'])) &&
    !empty(trim($_POST['rubro'])) &&
    !empty(trim($_POST['descripcion'])) &&
    !empty(trim($_POST['precio-unitario'])) &&
    !empty(trim($_POST['id-proveedor']))
    ) 
    {   
        
        $id      = $_POST['id'];
        $nom     = $_POST['nombre'];
        $rub     = $_POST['rubro'];
        $desc    = $_POST['descripcion'];
        $precio  = $_POST['precio-unitario'];
        $id_prov = $_POST['id-proveedor'];
        $valido  = false;

        switch($_POST){
            case ( is_string($nom)  && preg_match("/[a-zA-Z ]+/", $nom) ):
            case ( is_string($rub)  && preg_match("/[a-zA-Z ]+/", $rub) ):
            case ( is_string($desc) && preg_match("/[a-zA-Z ]+/", $desc) ):
            case ( is_numeric($precio) && filter_var($precio, FILTER_VALIDATE_FLOAT) ):
            case ( is_numeric($id_prov)  && filter_var($id_prov, FILTER_VALIDATE_INT) ):
                $valido = true;
                break;
            default: 
                $valido = false;    
        }

        if($valido){
            $query = "UPDATE producto SET nombre='$nom', rubro='$rub', descripcion='$desc', 
            precio_unitario='$precio', id_proveedor='$id_prov' 
            WHERE `id_articulo`='$id'";
            $res = mysqli_query($conex,$query);
            
            echo $res ? "<h3>¡Actualización realizada!</h3>" : 
            "<h3>¡Error!. Ha habido un error con la base de datos al intentar actualizar los datos.</h3>";
        }

    }else{
        echo "<h3>¡Error!.</h3>";
    }

    header("refresh:3; url=mostrar.php" );

?>

<style>
    <?php include 'style.css'; ?>
</style>